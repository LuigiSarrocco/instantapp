La nostra Instant App permette di giocare a tris in modalita' multiplayer online.
Per fare cio' si appoggia sulla funzionalita' realtime database di Firebase.

Per provare l'app sono necessari almeno 2 dispositivi.
Per eseguire l'app in modalita' Instant e' necessario attivare in Android Studio l'opzione:
	Run > Edit Configurations > Deploy as instant app

Il file APK non permette di eseguire l'app in modalita' instant.

L'app e' stata testata sulle versioni android:
	- 5
	- 9
	- 10