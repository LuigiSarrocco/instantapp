package com.example.instantlode;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.Network;
import android.net.NetworkRequest;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

/**
 * Activity principale.
 */
public class MainActivity extends AppCompatActivity {

    public static final int REQUEST_MATCHMAKING = 10;
    public static final int REQUEST_GAME = 11;
    private static final String SAVED_NAME = "NAME";

    private TextView tvPlayerName;
    private String playerName;

    private ConnectivityManager connectivityManager;
    private ConnectivityManager.NetworkCallback networkCallback;
    private AlertDialog alertDialog;

    /**
     * Il metodo onCreate imposta il layout e crea il listener per il controllo della connessione.
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        tvPlayerName = findViewById(R.id.player_name);

        //setto il listener per la connessione
        connectivityManager = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        networkCallback = new ConnectivityManager.NetworkCallback() {
            @Override
            public void onLost(Network network) {
                AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this, R.style.Theme_AppCompat_Dialog_Alert)
                        .setTitle("No Internet").setMessage("Connection Lost").setCancelable(false);
                // Faccio apparire il dialog
                alertDialog = builder.show();
            }

            @Override
            public void onAvailable(Network network) {
                // Prima che l'utente accetta il dialog di chiusura, se ritorna la connessione, chiudo il dialog
                if (alertDialog != null)
                    alertDialog.dismiss();
            }
        };
    }

    /**
     * Riceve i parametri di ritorno da Matchmaking e avvia GameActivity.
     *
     * @param requestCode
     * @param resultCode
     * @param data
     */
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == REQUEST_MATCHMAKING) {
            if (resultCode == Matchmaking.MATCH_FOUND) {
                // una volta trovata la partita lancio GameActivity
                Intent intent = new Intent(this, GameActivity.class);
                intent.putExtra(Matchmaking.GAME_KEY, data.getStringExtra(Matchmaking.GAME_KEY));
                intent.putExtra(Matchmaking.PLAYER_TAG, data.getIntExtra(Matchmaking.PLAYER_TAG, 0));
                intent.putExtra(Matchmaking.NICKNAME, data.getStringExtra(Matchmaking.NICKNAME));
                startActivityForResult(intent, REQUEST_GAME);
            }
            if (resultCode == Matchmaking.MATCH_NOT_FOUND) {
                // messaggio di errore
            }
        }
        if (requestCode == REQUEST_GAME) {
            // forse inutile
        }
    }

    /**
     * Metodo invocato dal pulsante 'play' che lancia l'activity per il matchmaking.
     * @param view
     */
    public void play(View view) {
        Intent intent = new Intent(this, Matchmaking.class);
        intent.putExtra(Matchmaking.NICKNAME, playerName);
        startActivityForResult(intent, REQUEST_MATCHMAKING);
    }

    /**
     * Metodo invocato dal pulsante 'edit' che lancia il dialog per midificare il nickname.
     * @param view
     */
    public void edit_name(View view) {
        NicknameDialog dialog = new NicknameDialog();
        dialog.setCancelable(false);
        dialog.show(getSupportFragmentManager(), "dialog");
    }

    /**
     * Metodo che imposta il nuovo nickname e chiede di modificarlo in caso non sia valido.
     * @param name Stringa contenente il nuovo nickname.
     */
    public void onNewName(String name) {
        playerName = name;
        tvPlayerName.setText(name);
        if (playerName.length() == 0) {
            NicknameDialog dialog = new NicknameDialog();
            dialog.setCancelable(false);
            dialog.show(getSupportFragmentManager(), "dialog");
        }
    }

    /**
     * Recupera il nickname e imposto il listener per il controllo della connessione.
     */
    @Override
    protected void onResume() {
        super.onResume();
        NetworkRequest networkRequest = new NetworkRequest.Builder().build();
        //controllo se all'inizio ho una connessione attiva
        if(!connectivityManager.isDefaultNetworkActive()){
            AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this, R.style.Theme_AppCompat_Dialog_Alert)
                    .setTitle("No Internet").setMessage("Connection Lost").setCancelable(false);
            // Faccio apparire il dialog
            alertDialog = builder.show();
        }

        connectivityManager.registerNetworkCallback(networkRequest, networkCallback);

        SharedPreferences preferences = getPreferences(MODE_PRIVATE);
        playerName = preferences.getString(SAVED_NAME, "");
        onNewName(playerName);
    }

    /**
     * Salva il nickname e rimuove il listener per il controllo della connessione.
     */
    @Override
    protected void onPause() {
        super.onPause();
        SharedPreferences preferences = getPreferences(MODE_PRIVATE);
        SharedPreferences.Editor editor = preferences.edit();
        // Store status in the preferences
        editor.putString(SAVED_NAME, playerName);
        // Commit to storage synchronously
        editor.commit();
        // Scollego il network callback quando l'activity viene messa in pausa
        connectivityManager.unregisterNetworkCallback(networkCallback);
    }

    public String getPlayerName() {
        return playerName;
    }

}
