package com.example.instantlode;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.Network;
import android.net.NetworkRequest;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.MutableData;
import com.google.firebase.database.Transaction;
import com.google.firebase.database.ValueEventListener;

/**
 * Matchmaking e' una activity che si occupa di trovare un secondo giocatore e di creare la partita
 * in cui i due utenti possono competere.
 */

public class Matchmaking extends AppCompatActivity {

    public static final int MATCH_FOUND=1;
    public static final int MATCH_NOT_FOUND=2;
    public static final int MATCH_STOPPED=3;
    public static final String NICKNAME = "nickname";
    public static final String GAME_KEY = "key";
    public static final String PLAYER_TAG = "tag";

    private ConnectivityManager connectivityManager;
    private ConnectivityManager.NetworkCallback networkCallback;
    private AlertDialog alertDialog;

    private View bStop; //bottone per terminare il matchmaking
    private TextView tvWaiting;

    private String nickname;
    private int player_tag = -1;
    private String gameKey = null;
    private DatabaseReference matchmakerReference;
    private DatabaseReference gamesReference;
    private DatabaseReference matchReference = null;
    private ValueEventListener matchListener;

    /**
     * Il metodo onCreate imposta il layout, imposta le reference al database, crea il listener
     * per il controllo della connessione e chiama il metodo findMatch().
     * @param savedInstanceState
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_matchmaking);
        bStop=findViewById(R.id.stopMatchmaking);
        tvWaiting=findViewById(R.id.waiting_text);
        nickname = getIntent().getExtras().getString(NICKNAME);
        //listener per il bottone fine-matchmaking
        bStop.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                tvWaiting.setText("Matchmaking stopped");
                setResult(MATCH_STOPPED);
                finish();
            }
        });
        //setto il listener per la connessione
        connectivityManager = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        networkCallback = new ConnectivityManager.NetworkCallback() {
            @Override
            public void onLost(Network network) {
                AlertDialog.Builder builder = new AlertDialog.Builder(Matchmaking.this, R.style.Theme_AppCompat_Dialog_Alert)
                        .setTitle("No Internet").setMessage("Connection Lost").setCancelable(false);
                // Faccio apparire il dialog
                alertDialog=builder.show();
            }

            @Override
            public void onAvailable(Network network) {
                // Prima che l'utente accetta il dialog di chiusura, se ritorna la connessione, chiudo il dialog
                if (alertDialog != null)
                    alertDialog.dismiss();
            }
        };
        NetworkRequest networkRequest = new NetworkRequest.Builder().build();
        connectivityManager.registerNetworkCallback(networkRequest, networkCallback);
        // setto le reference al database
        FirebaseDatabase database = FirebaseDatabase.getInstance();
        matchmakerReference = database.getReference("Matchmaker");
        gamesReference = database.getReference("Games");
        // inizio il matchmaking
        findMatch();
    }

    /**
     * Il metodo onPause() e' stato sovrascritto per eliminare dal database la partita creata,
     * ancora in attesa di un secondo giocatore, nel caso venisse interrotto il matchmaking.
     * Inoltre rimuove il listener per il controllo della connessione.
     */
    @Override
    protected void onPause() {
        super.onPause();
        // Scollego il network callback quando l'activity viene messa in pausa
        connectivityManager.unregisterNetworkCallback(networkCallback);
        // prima chiudo il listener
        if (matchListener != null)
            matchReference.child("player2").removeEventListener(matchListener);
        // se chiudo l'activity senza aver trovato un secondo giocatore elimino la partita creata
        if (player_tag == -1 && matchReference != null) {
            matchmakerReference.runTransaction(new Transaction.Handler() {
                @Override // azioni mutual exlusive
                public Transaction.Result doTransaction(MutableData mutableData) {
                    if (mutableData.getValue(String.class).equals(gameKey)) {
                        // elimino la chiave della mia partita in modo che nessuno possa partecipare
                        mutableData.setValue("");
                        return Transaction.success(mutableData);
                    }
                    // se non ho trovato la mia chiave significa che qualcuno si e' gia' unito alla mia partita
                    return Transaction.abort();
                }
                @Override
                public void onComplete(DatabaseError databaseError, boolean commit, DataSnapshot dataSnapshot) {
                    if (commit)
                        matchReference.removeValue();
                    else {
                        /* PROBLEMA DA RISOLVERE
                           se sono qui significa che qualcuno si e' unito alla partita mentre abbandonavo il matchmaking
                           che si fa??? */
                        /* PROBLEMA RISOLTO
                           non elimino la partita ma solo il mio nome cosi all'altro giocatore apparira'
                           come se l'avessi abbandonata e sara' lui poi ad eliminarla */
                        matchReference.child("player1").setValue("");
                    }
                    finish();
                }
            });
        }
    }

    /**
     * Il metodo findMatch() controlla se e' gia' presente una partita in cui sia atteso un secondo giocatore,
     * leggendo il valore presente nel campo 'matchmaker' del database.
     * Se la trova prova ad unirsi chiamando il metodo findMatchSecondArriver().
     * Altrimenti chiama findMatchFirstArriver() che crea una nuova partita e resta in attesa di un secondo giocatore.
     */
    private void findMatch() {
        matchmakerReference.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                final String matchmaker = dataSnapshot.getValue(String.class);

                if (matchmaker.length() == 0) {
                    findMatchFirstArriver();
                } else {
                    findMatchSecondArriver(matchmaker);
                }
            }
            @Override
            public void onCancelled(DatabaseError databaseError) {}
        });
    }

    /**
     * Crea una partita in attesa di un secondo giocatore.
     * Una volta creata la partita la chiave che la identifica viene pubblicata in modo mutualmente esclusivo
     * nel campo 'matchmaker' del database. Se questa pubblicazione fallisce significa che qualcun'altro e' gia'
     * in attesa di un secondo giocatore e quindi posso eliminare la partita creata e unirmi come secondo giocatore
     * alla partita la cui chiave e' gia' stata pubblicata.
     */
    private void findMatchFirstArriver() {
        // creo e punto al nodo della partita
        matchReference = gamesReference.push();
        // inizializzo i campi della partita
        Game game = new Game(nickname);
        matchReference.setValue(game);
        // prendo la chiave autogenerata
        final String newMatchmaker = matchReference.getKey();
        gameKey = newMatchmaker;
        matchmakerReference.runTransaction(new Transaction.Handler() {
            @Override // azioni mutual exlusive
            public Transaction.Result doTransaction(MutableData mutableData) {
                if (mutableData.getValue(String.class).length() == 0) {  // se e' vuota
                    mutableData.setValue(newMatchmaker);                // setto il valore alla chiave autogenerata
                    return Transaction.success(mutableData);
                }
                // someone beat us to posting a game, so fail and retry later
                return Transaction.abort();
            }
            @Override
            public void onComplete(DatabaseError databaseError, boolean commit, DataSnapshot dataSnapshot) {
                if (commit) {
                    matchListener = new ValueEventListener() {
                        @Override
                        public void onDataChange(DataSnapshot dataSnapshot) {
                            if (dataSnapshot.getValue(String.class).length() != 0) {
                                gamesReference.removeEventListener(this);
                                player_tag = 0;
                                matchSuccess();
                            }
                        }
                        @Override
                        public void onCancelled(DatabaseError databaseError) {}
                    };
                    matchReference.child("player2").addValueEventListener(matchListener);
                }
                else {
                    matchReference.removeValue();
                    matchReference = null;
                    gameKey = null;
                    findMatch();
                }
            }
        });
    }

    /**
     * Metodo che prova ad unirsi ad una partita in attesa del secondo giocatore.
     * Prima di partecipare alla partita rimuove la chiave che la identifica dal campo 'matchmaker' del database.
     * Questa azione deve essere mutualmente esclusiva per garantire che solo un giocatore si unisca alla partita.
     * @param matchmaker Chiave identificativa della partita a cui ci si vuole unire
     */
    private void findMatchSecondArriver(final String matchmaker) {
        matchmakerReference.runTransaction(new Transaction.Handler() {
            @Override
            public Transaction.Result doTransaction(MutableData mutableData) {
                if (mutableData.getValue(String.class).equals(matchmaker)) {
                    mutableData.setValue("");         // impedisco agli altri di entrare
                    return Transaction.success(mutableData);
                }
                return Transaction.abort();     // qualcuno ci ha preceduto, riprovo
            }
            @Override
            public void onComplete(DatabaseError databaseError, boolean committed, DataSnapshot dataSnapshot) {
                if (committed) {
                    matchReference = gamesReference.child(matchmaker);
                    matchReference.child("player2").setValue(nickname);
                    gameKey = matchmaker;
                    player_tag = 1;
                    matchSuccess();
                } else {
                    findMatch();
                }
            }
        });
    }


    /**
     * Metodo chiamato per chiudere l'activity in caso di partita trovata.
     */
    private void  matchSuccess(){
        Intent intent = new Intent();
        intent.putExtra(GAME_KEY, gameKey);
        intent.putExtra(PLAYER_TAG, player_tag);
        intent.putExtra(NICKNAME, nickname); // fondamentale per il funzionamento con l'opzione "Don't keep activities"
        setResult(MATCH_FOUND, intent);
        finish();
    }


}
