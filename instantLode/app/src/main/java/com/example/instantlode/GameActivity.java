package com.example.instantlode;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.app.AlertDialog;

import android.content.Context;
import android.content.DialogInterface;
import android.net.ConnectivityManager;
import android.net.Network;
import android.net.NetworkRequest;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

/**
 * GameActivity gestisce la fase di gioco:
 * Mostra la griglia che rappresenta lo stato della partita,
 * comunica al database le mosse compiute dall'utente e
 * aggiorna lo stato della partita in base agli aggiornamenti ricevuti dal database.
 */
public class GameActivity extends AppCompatActivity {

    public static final int GAME_OVER = 20;
    public static final int DISCONNECTED = 21;

    private ConnectivityManager connectivityManager;
    private ConnectivityManager.NetworkCallback networkCallback;
    private AlertDialog alertDialog;

    private DatabaseReference matchReference;
    private ValueEventListener matchListener;

    private int player_tag;
    private boolean isMyTurn;
    private static final int[] XO = {7,8};
    private int[][] grid = {{0,0,0},
                            {0,0,0},
                            {0,0,0}};
    private String opponent_name = "";
    private String nickname;

    private ImageView[][] cell = new ImageView[3][3];
    private TextView opponentView;
    private TextView turnView;

    /**
     * Nel metodo onCreate viene impostato il layout, creato il listener che si occupa di ricevere
     * gli aggiornamenti dal database e il listener per il controllo della connessione.
     * @param savedInstanceState
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_game);
        // inizializzo i riferimenti alle view
        cell[0][0] = findViewById(R.id.cell00);
        cell[1][0] = findViewById(R.id.cell10);
        cell[2][0] = findViewById(R.id.cell20);
        cell[0][1] = findViewById(R.id.cell01);
        cell[1][1] = findViewById(R.id.cell11);
        cell[2][1] = findViewById(R.id.cell21);
        cell[0][2] = findViewById(R.id.cell02);
        cell[1][2] = findViewById(R.id.cell12);
        cell[2][2] = findViewById(R.id.cell22);
        opponentView = findViewById(R.id.opponent_name);
        turnView = findViewById(R.id.turn);
        // ottengo i parametri da Matchmaking
        String gamekey = getIntent().getExtras().getString(Matchmaking.GAME_KEY);
        player_tag = getIntent().getExtras().getInt(Matchmaking.PLAYER_TAG);
        nickname= getIntent().getExtras().getString(Matchmaking.NICKNAME);
        //setto il listener per la connessione
        connectivityManager = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        networkCallback = new ConnectivityManager.NetworkCallback() {
            @Override
            public void onLost(Network network) {
                AlertDialog.Builder builder = new AlertDialog.Builder(GameActivity.this, R.style.Theme_AppCompat_Dialog_Alert)
                        .setTitle("No Internet").setMessage("Connection Lost").setCancelable(false);
                // Faccio apparire il dialog
                alertDialog=builder.show();
            }

            @Override
            public void onAvailable(Network network) {
                // Prima che l'utente accetta il dialog di chiusura, se ritorna la connessione, chiudo il dialog
                if (alertDialog != null)
                    alertDialog.dismiss();
            }
        };
        // inizializzo le reference al database
        FirebaseDatabase database = FirebaseDatabase.getInstance();
        matchReference = database.getReference("Games").child(gamekey);
        // setto il listener
        matchListener = new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                // leggo il nome dell'altro giocatore
                int otag = (player_tag == 0) ? 2 : 1;
                opponent_name = dataSnapshot.child("player"+otag).getValue(String.class);
                // controllo se e' il mio turno
                int turn = dataSnapshot.child("turn").getValue(Integer.class).intValue();
                isMyTurn = (turn == player_tag);
                // aggiorno la griglia
                for (int i = 0; i < 3; i++) {
                    for (int j = 0; j < 3; j++) {
                        int v = dataSnapshot.child("x" + i + "y" + j).getValue(Integer.class).intValue();
                        grid[i][j] = v;
                    }
                }
                updateView();
                checkState();
            }
            @Override
            public void onCancelled(DatabaseError databaseError) {}
        };
    }

    /**
     * Nel metodo onResume viene impostato il listener del database e quello per il controllo della connessione.
     * Inoltre viene scritto il nome dell'utente nel campo apposito del databse
     * per comunicare all'altro giocatore che siamo attivi in partita.
     */
    @Override
    protected void onResume() {
        super.onResume();
        matchReference.addValueEventListener(matchListener);
        matchReference.child("player"+(player_tag+1)).setValue(nickname);
        NetworkRequest networkRequest = new NetworkRequest.Builder().build();
        connectivityManager.registerNetworkCallback(networkRequest, networkCallback);
    }

    /**
     * Il metodo onPause si occupa di:
     * eliminare il nome dell'utente per comunicare che non e' piu' attivo in partita,
     * rimuovere il listener dal database e quello del controllo della connessione
     * e, nel caso non ci siano piu' giocatori presenti in partita, di eliminarla dal databse.
     */
    @Override
    protected void onPause() {
        super.onPause();
        // cancello il mio nome dalla partita per segnalare che esco
        matchReference.child("player"+(player_tag+1)).setValue("");
        // Scollego il network callback quando l'activity viene messa in pausa
        connectivityManager.unregisterNetworkCallback(networkCallback);
        // rimuovo il listener
        matchReference.removeEventListener(matchListener);
        // devo rimuovere la partita dal database ma solo se sono l'ultimo giocatore a lasciarla
        if (opponent_name.length() == 0) {
            matchReference.removeValue();
            finish();
        }
    }


    /**
     * Aggiorna le view che mostrano lo stato della partita.
     */
    private void updateView() {
        // aggiorno la view
        if (opponent_name.length() != 0) opponentView.setText("Your opponent is: "+opponent_name);
        else opponentView.setText(R.string.left);
        if (isMyTurn) turnView.setText(R.string.your_turn);
        else turnView.setText(R.string.waiting);
        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < 3; j++) {
                if (grid[i][j] == XO[0]) cell[i][j].setImageResource(R.drawable.tris_x);
                else if (grid[i][j] == XO[1]) cell[i][j].setImageResource(R.drawable.tris_o);
            }
        }
    }

    /**
     * Invia al database la mossa compiuta dall'utente.
     * @param x I parametri x e y identificano la cella in cui l'utente ha mosso.
     * @param y I parametri x e y identificano la cella in cui l'utente ha mosso.
     */
    private void playerMove(int x, int y) {
        if ((grid[x][y] == 0) && (isMyTurn)) {
            matchReference.child("x" + x + "y" + y).setValue(XO[player_tag]);
            matchReference.child("turn").setValue((player_tag == 0) ? 1 : 0);
        }
    }

    /**
     * Metodo che controlla se la partita e' terminata e chi ha vinto.
     */
    private void checkState() {
        int x = XO[0];
        int o = XO[1];
        boolean isDraw = true;
        boolean xWin = false;
        boolean oWin = false;
        int k = 0;
        for(int i = 0; i < 3; i++) {
            k = grid[i][0] + grid[i][1] + grid[i][2];
            if (k < Math.min(x, o) * 3) isDraw = false;
            if (k == x*3) xWin = true;
            if (k == o*3) oWin = true;
        }
        for (int i = 0; i < 3; i++) {
            k = grid[0][i] + grid[1][i] + grid[2][i];
            if (k == x*3) xWin = true;
            if (k == o*3) oWin = true;
        }
        k = grid[0][0] + grid[1][1] + grid[2][2];
        if (k == x*3) xWin = true;
        if (k == o*3) oWin = true;
        k = grid[0][2] + grid[1][1] + grid[2][0];
        if (k == x*3) xWin = true;
        if (k == o*3) oWin = true;
        if (xWin)
            if(player_tag == 1) gameOver(getString(R.string.u_lose));
            else gameOver(getString(R.string.u_win));
        else if (oWin)
                if(player_tag == 0) gameOver(getString(R.string.u_lose));
                else gameOver(getString(R.string.u_win));
        else if(isDraw) gameOver(getString(R.string.draw));
    }

    /**
     * Genera il dialog di fine partita mostrando la stringa s.
     * @param s Stringa che indica come si e' conclusa la partita.
     */
    private void gameOver(String s) {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage(s)
                .setTitle(R.string.gameover)
                .setPositiveButton(R.string.quit, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        setResult(GAME_OVER);
                        finish();
                    }
                });
        // Create the AlertDialog object and show it
        AlertDialog dialog=builder.create();
        dialog.setCancelable(false);
        dialog.setCanceledOnTouchOutside(false);
        dialog.show();
    }

    /**
     * Il metodo onKeyUp e' stato sovrascritto per mostrare un dialog prima di abbandonare la GameActivity
     * alla pressione (in realta' al rilascio) del pulsante 'back'.
     * @param keyCode Codice identificativo del pulsante che e' stato rilasciato.
     * @param event
     * @return
     */
    @Override
    public boolean onKeyUp(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            quit_dialog();
            return true;
        }
        return super.onKeyDown(keyCode, event);
    }

    /**
     * Genera il dialog che chiede la conferma prima che la partita venga abbandonata.
     */
    private void quit_dialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage("Are you sure to quit?")
                .setTitle(R.string.quit_game)
                .setPositiveButton(R.string.quit, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        setResult(DISCONNECTED);
                        finish();
                    }
                })
                .setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {}
                });
        // Create the AlertDialog object and show it
        AlertDialog dialog = builder.create();
        dialog.show();
    }


    // lunga lista di metodi chiamati dalle view
    public void quit_game(View view) {
        quit_dialog();
    }

    public void cell00(View view) { playerMove(0, 0); }
    public void cell10(View view) { playerMove(1, 0); }
    public void cell20(View view) { playerMove(2, 0); }
    public void cell01(View view) { playerMove(0, 1); }
    public void cell11(View view) { playerMove(1, 1); }
    public void cell21(View view) { playerMove(2, 1); }
    public void cell02(View view) { playerMove(0, 2); }
    public void cell12(View view) { playerMove(1, 2); }
    public void cell22(View view) { playerMove(2, 2); }

}
