package com.example.instantlode;

/**
 * La classe Game e' usata come 'stampo' per inizializzare i campi di una partita nel database.
 */
public class Game {
    private int [][] grid = {{0, 0, 0}, {0, 0, 0}, {0, 0, 0}};
    private String player1;
    private String player2;
    private int turn = 0;

    public Game(String player1) {
        this.player1 = player1;
        this.player2 = "";
    }

    public int getX0y0() {
        return grid[0][0];
    }

    public int getX0y1() {
        return grid[0][1];
    }

    public int getX0y2() {
        return grid[0][2];
    }

    public int getX1y0() {
        return grid[1][0];
    }

    public int getX1y1() {
        return grid[1][1];
    }

    public int getX1y2() {
        return grid[1][2];
    }

    public int getX2y0() {
        return grid[2][0];
    }

    public int getX2y1() {
        return grid[2][1];
    }

    public int getX2y2() {
        return grid[2][2];
    }

    public String getPlayer1() {
        return player1;
    }

    public String getPlayer2() {
        return player2;
    }

    public int getTurn() {
        return turn;
    }
}
